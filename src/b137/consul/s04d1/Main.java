package b137.consul.s04d1;

public class Main {

    public static void main(String[] args) {
        Car firstCar = new Car();

        firstCar.setName("Mirage");
        firstCar.setBrand("Mitsubishi");
        firstCar.setYearOfMake(2013);

        firstCar.drive();

        System.out.println(firstCar.getName());
        System.out.println(firstCar.getBrand());
        System.out.println(firstCar.getYearOfMake());

        Car secondCar = new Car("Avanza", "Toyota", 2005);

        secondCar.drive();
    }
}
